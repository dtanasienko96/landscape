"Vim overall settings
"{
    colo farout
    set number
    set exrc
    set secure
    set nowrap
    set undodir       =~/.vim/.undo//
    set backupdir     =~/.vim/.backup//
    set directory     =~/.vim/.swp//
    set nocompatible            
    set expandtab
    set tabstop       =4
    set softtabstop   =4
    set shiftwidth    =4
"}


"Vundle plugin manager settings
"{
    set rtp+=~/.vim/bundle/Vundle.vim

    call vundle#begin()
    Plugin 'VundleVim/Vundle.vim'
    Plugin 'scrooloose/nerdtree'
    Plugin 'Valloric/YouCompleteMe'
    call vundle#end()            
"}

"YouCompleteMe settings
"{
    let g:ycm_server_python_interpreter = 'python2'
    let g:ycm_global_ycm_extra_conf = "./.ycm_extra_conf.py"
"}

"NERDTree settings
"{
    nn <Tab><CR> :NERDTreeToggle<CR>
    
    let g:NERDTreeDirArrowExpandable = '+'
    let g:NERDTreeDirArrowCollapsible = '-'
    let g:NERDTreeShowHidden = 1
    let g:NERDTreeMinimalUI = 1
    let g:NERDTreeQuitOnOpen = 1
    	
    autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
"}

"netrw settings
"{
    let g:netrw_liststyle = 3
    let g:netrw_banner = 0
    let g:netrw_browse_split = 4
    let g:netrw_winsize = 15
    let g:netrw_altv = 1
"}

source $VIMRUNTIME/defaults.vim
