#include "landscape_generator.h"

namespace gen 
{
    LandscapeGenerator:: LandscapeGenerator() : m_mutation_tasks()
    { 

    }

    LandscapeGenerator& LandscapeGenerator::add_mutation(const HeightMapMutator& mutator) 
    {
        m_mutation_tasks.push(mutator); 
        return *this;
    }

    LandscapeGenerator& LandscapeGenerator::add_mutation(HeightMapMutator&& mutator) 
    {
        m_mutation_tasks.emplace(std::move(mutator));
        return *this;
    }

    HeightMap LandscapeGenerator::build_height_map() 
    {
        return build_height_map(DEFAULT_WIDTH, DEFAULT_LENGTH);
    }

    HeightMap LandscapeGenerator::build_height_map(HeightMap::size_type width, HeightMap::size_type length)
    {
        HeightMap height_map(width, length);

        while (!m_mutation_tasks.empty()) 
        {
            {
                const auto& current_task = m_mutation_tasks.front();
                current_task(height_map);
            }

            m_mutation_tasks.pop();
        }

        return height_map;
    }
}
